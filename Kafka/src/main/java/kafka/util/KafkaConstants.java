package kafka.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class KafkaConstants {
    public static final String IBO_CONTAINER_FACTORY = "ibo-cnt-factory";

    public static final String IBO_CONSUMER = "ibo-cns";
    public static final String IBO_CONTAINER = "ibo-cntr";
    public static final String IBO_TOPIC_KEY = "ibo-topic";


}
