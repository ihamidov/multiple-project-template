package kafka.service;

import kafka.util.KafkaConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AcknowledgingMessageListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service(KafkaConstants.IBO_CONSUMER)
@RequiredArgsConstructor
@Slf4j
public class KafKaConsumerService implements AcknowledgingMessageListener<String, String> {
    private final Logger logger =
            LoggerFactory.getLogger(KafKaConsumerService.class);

    @KafkaListener(topics = "ibo-topic",
            groupId = "ibo-group-id")
    public void consume(String message) {
        if (message.contains("test")) {
            logger.info("Incompatible message {} ", message);
            throw new RuntimeException("Incompatible message " + message);
        }


        logger.info(String.format("Message recieved -> %s", message));
    }

    @Override
    public void onMessage(ConsumerRecord<String, String> data, Acknowledgment acknowledgment) {

    }
}