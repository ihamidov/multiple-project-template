package kafka.service;

//import brave.Tracer;
//import lombok.RequiredArgsConstructor;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.slf4j.MDC;
//import org.springframework.kafka.listener.AcknowledgingMessageListener;
//import org.springframework.kafka.support.Acknowledgment;

//import java.util.Objects;

//@RequiredArgsConstructor
//public class AckTracingMessageListener implements AcknowledgingMessageListener<String, String> {

//    private final Tracer tracer;
//    private final AcknowledgingMessageListener<String, String> messageListener;
//
//    @Override
//    public void onMessage(ConsumerRecord<String, String> data, Acknowledgment acknowledgment) {
//        final var span = tracer.newTrace();
//        span.start();
//        MDC.put("spanId","" + span.context().spanId());
//        MDC.put("traceId", span.context().traceIdString());
//        if (Objects.nonNull(data) && Objects.nonNull(data.key())) {
//            MDC.put("uid", Objects.toString(data.key()));
//        }
//
//        messageListener.onMessage(data, acknowledgment);
//
//        span.finish();
//        MDC.remove("spanId");
//        MDC.remove("traceId");
//        MDC.remove("uid");
//    }
//}
