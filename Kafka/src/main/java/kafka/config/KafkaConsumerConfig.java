package kafka.config;

import kafka.service.KafKaConsumerService;
import kafka.util.KafkaConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.util.backoff.FixedBackOff;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerConfig {
    private static final String IGNORED = "ignored";

    private KafkaTemplate<String, Object> kafkaTemplate;
    @Value("${spring.kafka.consumer.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${spring.kafka.consumer.group-id}")
    private String groupId;

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(
                ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                org.apache.kafka.common.serialization.StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                org.apache.kafka.common.serialization.StringDeserializer.class);
//        props.putAll(kafkaConfigModel.getSslProperties(resourceLoader));

        log.info("Consumer config is working");
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean(KafkaConstants.IBO_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<String, String> concurrentKafkaListenerContainer()
            throws IOException {
        ConsumerFactory<String, String> properties = consumerFactory();
        ConcurrentKafkaListenerContainerFactory<String, String> listenerContainerFactory =
                new ConcurrentKafkaListenerContainerFactory<>();
        listenerContainerFactory.setConsumerFactory(properties);
        return listenerContainerFactory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory
                = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        factory.setRetryTemplate(kafkaRetry());
        factory.setRecoveryCallback(retryContext -> {
            ConsumerRecord consumerRecord = (ConsumerRecord) retryContext.getAttribute("record");
            log.info("Recovery is called for message {} ", consumerRecord.value());
            return Optional.empty();
        });
        // Comment the RecordFilterStrategy if Filtering is not required
        factory.setRecordFilterStrategy(record -> record.value().contains(IGNORED));
        return factory;
    }

    @Bean(KafkaConstants.IBO_CONTAINER)
    public ConcurrentMessageListenerContainer<String, String> testEventContainer(
            @Qualifier(KafkaConstants.IBO_CONSUMER) KafKaConsumerService consumerService,
            @Qualifier(KafkaConstants.IBO_CONTAINER_FACTORY) ConcurrentKafkaListenerContainerFactory<String, String> containerFactory
    ) {
        log.info("container is working");
        ConcurrentMessageListenerContainer<String, String> container = containerFactory
                .createContainer("ibo-topic");
        container.setAutoStartup(true);
        container.setBeanName(KafkaConstants.IBO_CONTAINER);
        container.setConcurrency(2);
        container.setupMessageListener(consumerService);
        return container;
    }
//    private AckTracingMessageListener wrapInTracer(AcknowledgingMessageListener<String, String> messageListener) {
//        return new AckTracingMessageListener(tracer, messageListener);
//    }
public RetryTemplate kafkaRetry(){
    RetryTemplate retryTemplate = new RetryTemplate();
    FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
    fixedBackOffPolicy.setBackOffPeriod(5*1000l);
    retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
    SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
    retryPolicy.setMaxAttempts(4);
    retryTemplate.setRetryPolicy(retryPolicy);
    return retryTemplate;
}
}
