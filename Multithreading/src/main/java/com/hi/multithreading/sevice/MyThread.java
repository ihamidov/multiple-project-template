package com.hi.multithreading.sevice;

import org.springframework.stereotype.Service;

@Service
public class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("Thread name " + Thread.currentThread().getName());
    }
}
