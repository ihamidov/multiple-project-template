package com.hi.multithreading.sevice;

import org.springframework.stereotype.Service;

@Service
public class MyRunnableThread implements Runnable {
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread name " + Thread.currentThread().getName());
    }
}
