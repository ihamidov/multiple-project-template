package com.hi.multithreading.sevice;

public class ExampleService {
    public static void main(String[] args) {
        Table table = new Table();
        MyThread1 myThread1 = new MyThread1(table);
        MyThread2 myThread2 = new MyThread2(table);
        myThread1.start();
        myThread2.start();
    }
}

class Table {
    /*synchronized*/ void printTable(int n) {
        synchronized (this) {

        }
        for (int i = 1; i <= 5; i++) {
            System.out.println("Data: " + n * i);
            try {
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                System.out.println("ERROR: " + e);
            }
        }
    }
}

class MyThread1 extends Thread {
    Table table;

    MyThread1(Table table) {
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(5);
    }
}

class MyThread2 extends Thread {
    Table table;

    MyThread2(Table table) {
        this.table = table;
    }

    @Override
    public void run() {
        table.printTable(100);
    }
}
