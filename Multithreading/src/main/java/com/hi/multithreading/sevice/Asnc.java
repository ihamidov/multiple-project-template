package com.hi.multithreading.sevice;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class Asnc {

    @Qualifier("iboExecutor")
    private final TaskExecutor taskExecutor;

    public Asnc(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }


    public CompletableFuture<String> run(int i) {
        return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("T " + i + Thread.currentThread().getName());
                    return "T " + i + Thread.currentThread().getName();
                }, taskExecutor);
    }

    @Async("iboExecutor")
    public void run2(int i) {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("T " + i + Thread.currentThread().getName());
    }
}