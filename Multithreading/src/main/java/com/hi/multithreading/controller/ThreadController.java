package com.hi.multithreading.controller;

import com.hi.multithreading.sevice.Asnc;
import com.hi.multithreading.sevice.MyThread;
import com.hi.multithreading.sevice.MyRunnableThread;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Multi threading tag")
public class ThreadController {
    @Autowired
    private Asnc asnc;

    @GetMapping("/mytread")
    @ApiResponse(description = "responce voiddi", responseCode = "200")
    @Operation(description = "This api use Runnable ",summary = "This api use Runnable!")
    public void mytread() {
        for (int i = 0; i < 20; i++) {
            MyThread thread = new MyThread();
            thread.start();
        }
    }

    @GetMapping("/tread")
    public void tread() {
        for (int i = 0; i < 20; i++) {
            Thread tr = new Thread(new MyRunnableThread());
            tr.start();
        }
    }

    @GetMapping("/asnc")
    public void asnc() {
        System.out.println("controller is working");
        for (int i = 0; i < 20; i++) {
            asnc.run(i);
        }
    }

    @GetMapping("/asnc2")
    public void asnc2() {
        System.out.println("controller is working");
        for (int i = 0; i < 20; i++) {
            asnc.run2(i);
        }
    }
}
