package com.hi.multithreading;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "Multi threading services",
                description = "That is about multi threading",
                version = "v1"
        )
)
public class MultithreadingApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultithreadingApplication.class, args);
    }

}
