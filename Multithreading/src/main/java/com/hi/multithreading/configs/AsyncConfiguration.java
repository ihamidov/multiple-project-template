package com.hi.multithreading.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfiguration {

    @Bean(name = "iboExecutor")
    public ThreadPoolTaskExecutor taskExecutor() {
        final var executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(12);
        executor.setThreadNamePrefix("ibo-thread-");
        executor.setRejectedExecutionHandler(
                (r, ex) -> System.out.println("The task was rejected, current count: {} "+ex.getTaskCount())
        );
        executor.initialize();
        return executor;
    }
}
