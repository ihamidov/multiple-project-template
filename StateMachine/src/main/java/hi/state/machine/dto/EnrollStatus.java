package hi.state.machine.dto;

import hi.state.machine.service.transitions.Cancel;
import hi.state.machine.service.transitions.Exam;
import hi.state.machine.service.transitions.Payment;

import java.util.Arrays;
import java.util.List;

public enum EnrollStatus {
    PENDING(Exam.NAME, Cancel.NAME),
    EXAM(Payment.NAME),
    CANCEL(),
    PAYMENT_PENDING(),
    ENROLLED(),
    FAIL(),
    SUCCESS();
    private final List<String> transitions;

    EnrollStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getTransitions() {
        return transitions;
    }

}
