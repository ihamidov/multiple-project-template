package hi.state.machine.controller;

import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.impl.EnrollmentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;

@RestController
@Slf4j
@RequiredArgsConstructor
public class EnrollmentController {
private final EnrollmentService enrollmentService;
    @PostMapping("/enroll")
    public EnrollmentDto get(@RequestBody EnrollmentDto enrollmentDto){
       return enrollmentService.save(enrollmentDto);

    }

    @GetMapping("/user/{id}")
    public EnrollmentDto getUser(@PathVariable Long id){
        return enrollmentService.getUserDetails(id);

    }


}
