package hi.state.machine;

import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.EnrollmentTransitionsService;
import hi.state.machine.service.impl.EnrollmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@Slf4j
public class StateMachineApplication implements CommandLineRunner {

    @Autowired
    private EnrollmentTransitionsService enrollmentTransitionsService;
    @Autowired
    private EnrollmentService enrollmentService;

    public static void main(String[] args) {
        SpringApplication.run(StateMachineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        EnrollmentDto enrollmentDto = EnrollmentDto.builder()
                .id(1L)
                .status(EnrollStatus.ENROLLED)
                .name("ibo")
                .build();
        EnrollmentDto enrolment = enrollmentService.save(enrollmentDto);
        log.info("Enrolment was saved {}", enrolment);
        List<String> allowedTransitions = enrollmentTransitionsService.getAllowedTransitions(enrolment.getId());
        log.info("Checked allowed states {}", allowedTransitions);
        var examState = enrollmentTransitionsService.transitionOrder(enrolment, "exam");
        log.info("Status changed to state {} for  {}", examState.getName(), examState);
//        var pendingStatus = enrollmentTransitionsService.transitionOrder(enrolment, "pending");
//        log.info("Status changed to state {} for  {}", pendingStatus.getName(), pendingStatus);
    }
}
