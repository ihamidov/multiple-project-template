package hi.state.machine.events;

import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.EnrollmentTransitionsService;
import org.springframework.context.ApplicationEvent;

public class EnrolmentStatusChangeEvent extends ApplicationEvent {
    private final String oldStatus;
    private final EnrollmentDto orderDto;

    public EnrolmentStatusChangeEvent(Object source, String oldStatus, EnrollmentDto orderDto) {
        super(source);
        this.oldStatus = oldStatus;
        this.orderDto = orderDto;
    }



    public String getOldStatus() {
        return oldStatus;
    }

    public EnrollmentDto getOrderDto() {
        return orderDto;
    }

}
