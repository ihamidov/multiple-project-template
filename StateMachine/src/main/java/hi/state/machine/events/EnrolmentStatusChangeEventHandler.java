package hi.state.machine.events;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class EnrolmentStatusChangeEventHandler implements ApplicationListener<EnrolmentStatusChangeEvent> {

    @Async
    @Override
    public void onApplicationEvent(EnrolmentStatusChangeEvent event) {
        log.info("Enrolment status changed from {} to {}", event.getOldStatus(), event.getOrderDto().getStatus());
//        sendNotificationEmail(event.getOrderDto());
    }

//    private void sendNotificationEmail(OrderDto orderDto) {
//        emailNotificationService.sendEmail(constructEmail(orderDto));
//    }
//
//    private Email constructEmail(OrderDto orderDto) {
//        final AccountDto accountById = accountService.findAccountById(orderDto.getAccount().getId());
//        accountById.getUser().getEmail();
//        return Email
//                .builder()
//                .to(accountById.getUser().getEmail())
//                .subject(orderDto.getStockId() + " order status update")
//                .text("Dear " + accountById.getUser().getUserName() + " , your order to " + orderDto.getType() + " "
//                        + orderDto.getStockId()
//                        + " for price " + orderDto.getMaxPrice() + " is " + orderDto.getStatus())
//                .build();
//    }

}
