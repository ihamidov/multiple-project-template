package hi.state.machine.repository;

import hi.state.machine.entity.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
}
