package hi.state.machine.service.impl;

import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.Action;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SendEmail implements Action {
    @Override
    public void applyProcess(EnrollmentDto enrollmentDto) {
        log.info("Sending email to {}",enrollmentDto.getName());
    }
}
