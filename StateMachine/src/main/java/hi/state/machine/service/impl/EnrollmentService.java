package hi.state.machine.service.impl;

import hi.state.machine.config.EnrollmentConfigration;
import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.entity.Enrollment;
import hi.state.machine.repository.EnrollmentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;

@Service
@Slf4j
@RequiredArgsConstructor
public class EnrollmentService {
    private final EnrollmentConfigration enrollmentConfigration;
    private final EnrollmentRepository repository;
    private final ModelMapper mapper;


    public EnrollmentDto save(EnrollmentDto enrollmentDto) {
        log.info("Enrollment will save {}", enrollmentDto);
        Enrollment enrollment = new Enrollment();
        enrollment.setStatus(EnrollStatus.PENDING);
        enrollment.setDate(LocalDateTime.now());
        enrollment.setName(enrollmentDto.getName());
        var a = repository.save(enrollment);
        System.out.println(a);
//        EnrollmentDto save = mapper.map(a, EnrollmentDto.class);
        enrollmentConfigration.getActions().stream().forEach(action -> action.applyProcess(enrollmentDto));
        return EnrollmentDto.builder()
                .name(a.getName())
                .date(a.getDate())
                .status(a.getStatus())
                .id(a.getId())
                .build();
    }

    public EnrollmentDto getUserDetails(Long id) {
        return mapper.map(repository.findById(id).get(), EnrollmentDto.class);
    }
}
