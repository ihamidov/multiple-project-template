package hi.state.machine.service;

import hi.state.machine.dto.EnrollmentDto;

public interface Action {
    public void applyProcess(EnrollmentDto enrollmentDto);
}
