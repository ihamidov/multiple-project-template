package hi.state.machine.service;

import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;

public interface EnrollmentTransitions {
    String getName();

    EnrollStatus getStatus();

    void applyProcessing(EnrollmentDto enrollmentDto);
}
