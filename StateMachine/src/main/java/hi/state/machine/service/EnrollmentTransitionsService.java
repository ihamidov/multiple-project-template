package hi.state.machine.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.entity.Enrollment;
import hi.state.machine.events.EnrolmentStatusChangeEvent;
import hi.state.machine.repository.EnrollmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class EnrollmentTransitionsService {
    private Map<String, EnrollmentTransitions> transitionsMap;
    //    private final ObjectMapper mapper;
    private final ModelMapper modelMapper;
    private final ApplicationEventPublisher publisher;
    private final EnrollmentRepository repository;

    public EnrollmentTransitionsService(ModelMapper modelMapper, List<EnrollmentTransitions> transitionsMap, ApplicationEventPublisher publisher, EnrollmentRepository repository) {
//        this.mapper = mapper;
        this.modelMapper = modelMapper;
        this.publisher = publisher;
        this.repository = repository;
        initTransitions(transitionsMap);
    }

    private void initTransitions(List<EnrollmentTransitions> enrollmentTransitions) {
        Map<String, EnrollmentTransitions> map = new HashMap<>();
        for (EnrollmentTransitions transition : enrollmentTransitions) {
            if (map.containsKey(transition.getName())) {
                throw new IllegalArgumentException("double key " + transition.getName());
            }
            map.put(transition.getName().toLowerCase(), transition);
        }
        this.transitionsMap = Collections.unmodifiableMap(map);
    }

    public List<String> getAllowedTransitions(Long id) {
        Enrollment enrollment = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Unknown enrollment" + id));
        System.out.println("I am here " + enrollment);
        return enrollment.getStatus().getTransitions();
    }

    @Transactional
    public EnrollmentDto transitionOrder(EnrollmentDto enrollmentDto, String transition) {
        EnrollmentTransitions transitions = transitionsMap.get(transition.toLowerCase());
        if (transitions == null) {
            throw new IllegalArgumentException("Unknown transition" + transition);
        }
        return repository.findById(enrollmentDto.getId())
                .map(enrollment -> {
                    checkAllowed(transitions, enrollment.getStatus());
//                    transitions.applyProcessing(EnrollmentDto.builder()
//                            .id(enrollment.getId())
//                            .status(enrollment.getStatus())
//                            .date(enrollment.getDate())
//                            .name(enrollment.getName())
//                            .build());
                    transitions.applyProcessing(modelMapper.map(enrollment, EnrollmentDto.class));
                    return updateStatus(enrollment, transitions.getStatus());
                })
                .map(i -> modelMapper.map(i, EnrollmentDto.class))
                .orElseThrow(() -> new IllegalArgumentException("Unknown enroll" + enrollmentDto.getId()));


    }

    private void checkAllowed(EnrollmentTransitions transitions, EnrollStatus status) {
        Set<EnrollStatus> allowedStatus = Stream.of(EnrollStatus.values())
                .filter(i -> i.getTransitions().contains(transitions.getName()))
                .collect(Collectors.toSet());
        if (!allowedStatus.contains(status)) {
            throw new RuntimeException("The transition from the " + status.name() + " status to the "
                    + transitions.getStatus().name() + " status is not allowed!");

        }
    }

    private Enrollment updateStatus(Enrollment enrollment, EnrollStatus status) {
        EnrollStatus existStatus = enrollment.getStatus();
        enrollment.setStatus(status);
        Enrollment updated = repository.save(enrollment);
        var event = new EnrolmentStatusChangeEvent(this, existStatus.name(), modelMapper.map(updated, EnrollmentDto.class));
        publisher.publishEvent(event);
        return updated;
    }


}
