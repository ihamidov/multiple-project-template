package hi.state.machine.service.impl;

import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.Action;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SendSms implements Action {
    @Override
    public void applyProcess(EnrollmentDto enrollmentDto) {
        log.info("Sending sms to {}",enrollmentDto.getName());
    }
}
