package hi.state.machine.service.transitions;

import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.EnrollmentTransitions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Exam implements EnrollmentTransitions {
    public final  static String NAME = "exam";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public EnrollStatus getStatus() {
        return EnrollStatus.EXAM;
    }

    @Override
    public void applyProcessing(EnrollmentDto enrollmentDto) {
        log.info("Applying pre processing for transition exam {} on enrollment {}", getStatus(), getName());

    }
}
