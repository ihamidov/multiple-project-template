package hi.state.machine.service.transitions;

import hi.state.machine.dto.EnrollStatus;
import hi.state.machine.dto.EnrollmentDto;
import hi.state.machine.service.EnrollmentTransitions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Fail implements EnrollmentTransitions {
    public final  static String NAME = "fail";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public EnrollStatus getStatus() {
        return EnrollStatus.FAIL;
    }

    @Override
    public void applyProcessing(EnrollmentDto enrollmentDto) {
        log.info("Applying pre processing for transition fail {} on enrollment {}", getStatus(), getName());

    }
}
