package hi.state.machine.config;

import hi.state.machine.service.Action;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Component
@ConfigurationProperties(prefix = "hi.enrollment")
public class EnrollmentConfigration {
    @Autowired
    private ApplicationContext applicationContext;
    private List<String> actionsNames;
    private List<Action> actions;

    @PostConstruct
    private void set() {
        actions = actionsNames.stream().map(i -> applicationContext.getBean(i, Action.class)).collect(Collectors.toList());

    }
}
