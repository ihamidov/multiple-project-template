package hi.file.service;

import hi.file.dto.Object;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

@Service
public class ExcelToJson {
    public void read() throws IOException {
        String line = "";
        String splitBy = ",";
        ArrayList<Object> list = new ArrayList<>();
        File fileDir = new File("/Users/shahin/Downloads/111fayda_ammount.csv");

        BufferedReader br = new BufferedReader(new InputStreamReader
                (new FileInputStream(fileDir), StandardCharsets.UTF_8));
        while ((line = br.readLine()) != null) {
            Object object = new Object();
            String[] excel = line.split(splitBy);
            object.setCif(excel[1]);
            object.setPoints(excel[3]);
            list.add(object);
        }
        String fileName = "/Users/shahin/Desktop/testing.json";
        write(fileName, list);
    }

    public void write(String fileName, ArrayList<Object> lines) {

        File file = new File(fileName);

        try (FileOutputStream fos = new FileOutputStream(file);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
             BufferedWriter writer = new BufferedWriter(osw)
        ) {

            for (Object line : lines) {
                String json = "{\"cif\":\"" + line.getCif() + "\",\"points\":\"" + line.getPoints() + "\"},";
                System.out.println(json);
                writer.append(json);
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
