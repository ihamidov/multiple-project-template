package hi.file.dto;

public class Object {
    private String cif;
    private String points;

    public void setCif(String cif) {
        this.cif = cif;
    }

    public void setPoints(String points) {
        this.points = points;
    }


    public String getCif() {
        return cif;
    }

    public String getPoints() {
        return points;
    }

    @Override
    public String toString() {
        return "Object{" +
                "cif='" + cif + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}


