package hi.file.controller;

import hi.file.service.Files;
import hi.file.service.ExcelToJson;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class FileController {
    private final ExcelToJson excel;
    private final Files files;

    public FileController(ExcelToJson excel, Files files) {
        this.excel = excel;
        this.files = files;
    }

    @GetMapping("/excel")
    public void excelFile() {
        System.out.println("controller is running");
        try {
            excel.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/txt")
    public void txtFile() {
        System.out.println("controller is running");
        try {
            files.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
