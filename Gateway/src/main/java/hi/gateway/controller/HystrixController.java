package hi.gateway.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
@RequiredArgsConstructor
public class HystrixController {

    @GetMapping("/fallback")
    public Mono<String> fallback() {
        return Mono.just("fallback");
    }
    @GetMapping("/account")
    public String accountFallBack() {
        return "Account service is not available!";
    }

    @GetMapping("/ticket")
    public String ticketFallBack() {
        return "Ticket service is not available!";
    }

}
