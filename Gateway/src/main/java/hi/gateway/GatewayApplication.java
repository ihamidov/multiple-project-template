package hi.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        System.out.println("is working");

        return builder.routes()
                .route(r -> r.path("/account")
                        .filters(i -> i.setPath("/guides"))
                        .uri("https://spring.io/"))
//                        .id("Gateway"))
//
//                .route(r -> r.path("/consumer/**")
//                        .uri("http://localhost:8082/")
//                        .id("consumerModule"))
                .build();
    }
}
