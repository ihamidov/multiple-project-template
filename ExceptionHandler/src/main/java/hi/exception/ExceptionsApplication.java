package hi.exception;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@SpringBootApplication
public class ExceptionsApplication implements CommandLineRunner {
    static String a = "D15605A68FF248FE803F1B050DC7C8E6";

    public static void main(String[] args) {
        SpringApplication.run(ExceptionsApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        System.out.println(func("ibrahim"));
        System.out.println(supp("Salam supplier"));
        cons("consumerdi");
        System.out.println(a);
        System.out.println(getHashedId(a));
        System.out.println("PREDICATE " + pre(a));

    }

    private static String getHashedId(String transfersDto) {

        return String.valueOf(
                Objects.hash(transfersDto, "ibrahim"));
    }

    private static Integer func(String transfersDto) {
        Function<String, Integer> func = x -> x.length();

        return func.apply(transfersDto);
    }

    private static String supp(String transfersDto) {
        Supplier<String> supp = () -> transfersDto;

        return supp.get();
    }

    private static void cons(String transfersDto) {
//        Consumer<String> cons = System.out::println;
        Consumer<String> cons = a -> {
            System.out.println(a.toUpperCase());
        };

        cons.accept(transfersDto);
    }

    private static boolean pre(String transfersDto) {
        Predicate<String> pr = String::isEmpty; // Creating predicate
        return pr.test(transfersDto);
    }



}
