package hi.exception.exception;

import hi.exception.dto.ErrorCodeEnum;
import lombok.Getter;

@Getter
public class CustomerNotFoundException extends RuntimeException {

    private final int code;
    private final String message;

//    public CustomerNotFoundException(String errorCodeEnum) {
//        super("message");
//                this.code = errorCodeEnum.getCode();
//        this.message = errorCodeEnum.getMessage();
//    }

    public CustomerNotFoundException(ErrorCodeEnum errorCodeEnum) {
        super(errorCodeEnum.getMessage());
        this.code = errorCodeEnum.getCode();
        this.message = errorCodeEnum.getMessage();
    }
}
