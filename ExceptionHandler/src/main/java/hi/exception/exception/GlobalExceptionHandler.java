package hi.exception.exception;

import hi.exception.dto.ErrorCodeEnum;
import hi.exception.dto.ErrorResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;


@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    @ExceptionHandler(CustomerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponseDTO handle(CustomerNotFoundException ex) {

        return ErrorResponseDTO.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDTO handle(MethodArgumentTypeMismatchException ex) {

        return ErrorResponseDTO.builder()
                .code(ErrorCodeEnum.VALIDATION_ERROR.getCode())
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDTO handle(MethodArgumentNotValidException ex) {
        var message = ex.getBindingResult().getFieldError().getDefaultMessage();
        var field = ex.getBindingResult().getFieldError().getField();
        return ErrorResponseDTO.builder()
                .code(ErrorCodeEnum.VALIDATION_ERROR.getCode())
                .message(field + " " + message + " " + ex.getMessage())
                .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponseDTO handle(Exception ex) {
        return ErrorResponseDTO.builder()
                .code(ErrorCodeEnum.UNKNOWN.getCode())
                .message(ErrorCodeEnum.UNKNOWN.getMessage())
                .build();
    }
}
