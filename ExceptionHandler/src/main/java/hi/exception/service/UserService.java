package hi.exception.service;

import hi.exception.dto.ErrorCodeEnum;
import hi.exception.dto.User;
import hi.exception.exception.CustomerNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    public String createUser(User user) {

        return user.getName() + user.getId() + " was created ";
    }

    public String getUserById(Integer id) {
        if (id == 1) {
            throw new CustomerNotFoundException(ErrorCodeEnum.BUSINESS);
        }
        return "User " + id;
    }
}
