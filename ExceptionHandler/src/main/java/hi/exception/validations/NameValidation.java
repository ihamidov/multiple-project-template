package hi.exception.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = NameValidator.class)
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface NameValidation {
    String message() default "This is not valid address";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}
