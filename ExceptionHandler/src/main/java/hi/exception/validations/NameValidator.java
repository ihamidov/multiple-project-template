package hi.exception.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class NameValidator implements ConstraintValidator<NameValidation,String> {
    List<String> name = Arrays.asList("Ibo","Ibrahim");
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return name.contains(value );
    }
}
