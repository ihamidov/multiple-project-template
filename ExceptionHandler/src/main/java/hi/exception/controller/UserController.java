package hi.exception.controller;

import hi.exception.dto.User;
import hi.exception.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@RestController
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/get")
    public String getUser(@RequestParam @Valid @Positive Integer id) {//@Valid @NotNull

        return service.getUserById(id);
    }

    @PostMapping("/create")
    public String createUser(@RequestBody @Valid User user) {//validation dependencyni elave etmek laimdir

        return service.createUser(user);
    }
}
