package hi.exception.dto;

import hi.exception.validations.NameValidation;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
@Builder
public class User {
    @Positive
    private int id;
    @NotBlank(message = "details is null")
    @NameValidation(message = "Name must be Ibo or Ibrahim")
    private String name;
}
