package hi.exception.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public enum ErrorCodeEnum {
    BUSINESS(001,"Business"),
    VALIDATION_ERROR(002,"Not error code"),
    TECHNICAL(003,"Technical"),
    UNKNOWN(004,"Unknown");
    private final int code;
    private final String message;

    ErrorCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
