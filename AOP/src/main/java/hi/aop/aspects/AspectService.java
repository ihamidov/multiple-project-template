package hi.aop.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.StringJoiner;

@Aspect
@Slf4j
@Component
public class AspectService {
    @Before("execution(* hi.aop.servive.*.sendMessage(..))")
    public void sendMessageBefore(JoinPoint joinPoint){
        log.info("Message get before main method: {}", joinPoint.getArgs()[0]);
        log.info("Signature : {}", joinPoint.getSignature());
    }

    @After("execution(* hi.aop.servive.*.sendMessage(..))")
    public void sendMessageAfter(JoinPoint joinPoint){
        log.info("Message get after main method: {}", joinPoint.getArgs()[0]);
        log.info("Signature : {}", joinPoint.getSignature());
    }


        @AfterThrowing(value = "execution(* hi.aop.servive.MessageService.sendMessage(..))", throwing = "throwable")
        public void logAfterThrowing(JoinPoint joinPoint, Throwable throwable) {

            throwable = Objects.nonNull(throwable.getCause()) ? throwable.getCause() : throwable;

            final StackTraceElement stackTraceElement = throwable.getStackTrace()[0];

            StringJoiner result = new StringJoiner(",");
            joinPoint.getClass();
            result.add("{\"method_name\":" + "\"" + joinPoint.getSignature().getName() + "\"")
                    .add("\"exception_type\":" + "\"" + throwable.getClass() + "\"")
                    .add("\"message\":" + "\"" + throwable.getMessage() + "\"")
                    .add("\"line_number\":" + "\"" + stackTraceElement.getLineNumber() + "\"")
                    .add("\"class_name\":" + "\"" + stackTraceElement.getClassName() + "\"" + "}")
                    .add("\"method_name\":" + "\"" + stackTraceElement.getMethodName() + "\"" + "}");

            throw new RuntimeException();
        }
}
