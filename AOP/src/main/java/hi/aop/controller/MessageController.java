package hi.aop.controller;

import hi.aop.servive.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;

    @PostMapping("/message/{param}")
    public String getMessage(@PathVariable String param){
       return messageService.sendMessage(param);
    }
}
