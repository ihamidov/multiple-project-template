package hi.cache.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {
    @Id
    private String id;
    private String title;

    public Book(String isbn, String title) {
        this.id = isbn;
        this.title = title;
    }

    public Book() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" + "isbn='" + id + '\'' + ", title='" + title + '\'' + '}';
    }

}
