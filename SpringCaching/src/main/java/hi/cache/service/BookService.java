package hi.cache.service;

import hi.cache.dto.Book;
import hi.cache.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Slf4j
@Service
public class BookService {
    private final BookRepository booksRepository;

    @CachePut(cacheNames = "books", key = "#book.id")
    public void insertBookAndCache(Book book) {
        log.info("Save book {} to DB", book.getId());
        booksRepository.save(book);
        log.info("Book {} already saved", book);
    }

//    @Cacheable(value="student", condition="#name.length<20")
    @Cacheable(cacheNames = "books", key = "#id")
    public Book findDataFromCache(String id) {
        simulateSlowService();
        log.info("Find book {} from DB", id);
        var book = booksRepository.findById(id);
        log.info("Book {} found", book.get());
        return book.get();
    }

    @CacheEvict(cacheNames = "books", key = "#id")
    public void deleteDataAndEvictCache(String id) {
        log.info("Delete book {} from DB ", id);
        booksRepository.findById(id);
        log.info("Book {} already deleted", id);
    }

    private void simulateSlowService() {
        try {
            long time = 5000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
