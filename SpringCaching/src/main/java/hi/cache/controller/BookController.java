package hi.cache.controller;

import hi.cache.dto.Book;
import hi.cache.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping("/insert")
    public void saveBook(@RequestBody Book book){
        bookService.insertBookAndCache(book);
    }

    @GetMapping("/find")
    public Book getBook(@RequestParam String id){
        return bookService.findDataFromCache(id);
    }

    @DeleteMapping("/delete")
    public void deleteBook(@RequestParam String id){
         bookService.deleteDataAndEvictCache(id);
    }
}
