package com.hi.websocket;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.util.Locale;

import static java.time.format.TextStyle.FULL_STANDALONE;

@SpringBootApplication
public class WebSocketApplication implements CommandLineRunner {
    public static final String DATE_STRING = "%s %d";

    public static void main(String[] args) {
        SpringApplication.run(WebSocketApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(getDateString(LocalDateTime.now(),Locale.ENGLISH));
    }

    public String getDateString(LocalDateTime now, Locale locale) {
        return String.format(DATE_STRING, now.getMonth().getDisplayName(FULL_STANDALONE, locale), now.getYear());
    }
}
