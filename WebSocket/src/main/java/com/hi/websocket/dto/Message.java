package com.hi.websocket.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Message {
    private String sender;
    private String message;
}
